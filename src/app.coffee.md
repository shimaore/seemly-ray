    {S} = Surplus = require 'surplus'
    SArray = (require 's-array').default
    cx = require 'classnames'
    data = (require 'surplus-mixin-data/index.js').default
    IO = require 'socket.io-client'

    io = IO.connect()
    io.on 'ready', ->
      status 'Ready'
      commands.forEach ({name}) -> io.emit "status #{name}"
      return

    io.on 'stdout', (id,data) -> consoles[id].push {t:'stdout',data}
    io.on 'stderr', (id,data) -> consoles[id].push {t:'stderr',data}
    io.on 'exe error', (id,err) ->
      consoles[id].push {t:'error',data:err}
      errored[id] true
      running[id] false
      return
    io.on 'exe close', (id,code,signal) ->
      running[id] false
      switch
        when code is 0
          consoles[id].push {t:'comment',data:"(closed OK)"}
        when signal is 'SIGTERM' # or whatever we `exe.kill` with
          consoles[id].push {t:'comment',data:"(terminated #{code})"}
        else
          consoles[id].push {t:'comment',data:"(failed #{code} #{signal})"}
          errored[id] true
      return
    io.on 'exe status', (id,r) ->
      console.log 'exe status', id, r
      running[id] r
      return

    status = S.data ''
    commands = [
      {name: 'date', run: '/bin/date'}
      {name: 'kill all jack', run: 'killall jackd'}
      {name: 'jack', run: '/usr/bin/jackd -ddummy -r48000 -p256'}
      {name: 'midi', run: 'a2jmidid', options: env: HOME: '/srv/home/stephane'}
      {name: 'PCH-in', run: 'zita-a2j -j PCH-in -d hw:PCH'}
      {name: 'USB-in', run: 'zita-a2j -j USB-in -d hw:CODEC'}
      {name: 'PCH-out', run: 'zita-j2a -j PCH-out -d hw:PCH'}
      {name: 'USB-out', run: 'zita-j2a -j USB-out -d hw:CODEC'}
    ]
    consoles = {}
    running  = {}
    errored  = {}

    module.exports = ->
      <div>
        <span>Status: {status}</span>

        {
        commands.map ({name,run,options},i) ->
          consoles[name] = SArray []
          running[name] = S.value false
          errored[name] = S.value false
          show_log = S.value false

          <div class="command">
            <button class={cx running: running[name](), errored: errored[name]()} onclick={ ->
              if running[name]()
                io.emit "kill #{name}"
              else
                [command,args...] = run.split /\s+/g
                running[name] true
                errored[name] false
                consoles[name].splice 0
                consoles[name].push {t:'comment',data:run}
                io.emit 'start', name, command, args, options
            }>{name}</button>
            <input type="checkbox" fn={data show_log} />
            {
            if show_log()
              <pre>{consoles[name].map ({data}) -> "#{data}\n"}</pre>
            }
          </div>
        }

        <style>
        {
          '''
          .command {
            display: inline;
          }
          .command button {
            width: 100px;
            height: 100px;
          }
          .command .running {
            background-color: green;
          }
          .command .errored{
            background-color: red;
          }
          '''
        }
        </style>
      </div>
