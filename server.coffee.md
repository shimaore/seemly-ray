    app = do Express = require 'express'
    server = (require 'http').Server app
    io = (require 'socket.io') server

    server.listen 8080

    {spawn} = require 'child_process'

    app.get '/', (req,res) -> res.sendFile "#{__dirname}/index.html"
    app.use Express.static 'dist'
    app.get '/client.bundle.js', (req,res) -> res.sendFile "#{__dirname}/dist/client.bundle.js"
    io.on 'connection', (socket) ->
      socket.emit 'ready'
      socket.on 'start', (id,command,args,options) ->
        {env} = options if options?
        env ?= {}
        exe = spawn command, args, {env}
        running = true
        socket.on "stdin #{id}", (data) -> exe.stdin.write data
        socket.on "end #{id}", -> exe.stdin.end()
        socket.on "kill #{id}", (signal) -> exe.kill signal

Note: won't work (meant to be used on Reload) because we establish a new connection.
Need to rewrite the server so that it has more state across connections.

        socket.on "status #{id}", ->
          console.log 'status', id, running
          socket.emit 'exe status', id, running
        exe.stdout.on 'data', (data) -> socket.emit 'stdout', id, data.toString 'utf8'
        exe.stderr.on 'data', (data) -> socket.emit 'stderr', id, data.toString 'utf8'
        exe.on 'close', (code,signal) ->
          socket.emit 'exe close', id, code, signal
          running = false
        exe.on 'error', (err) ->
          socket.emit 'exe error', id, err.toString()
          running = false

      return
