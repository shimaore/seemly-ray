    webpack = require 'webpack'
    TerserPlugin = require 'terser-webpack-plugin'
    path = require 'path'

    module.exports =
      target: 'electron-renderer' # see https://stackoverflow.com/questions/48476061/electron-and-typescript-fs-cant-be-resolved
      entry:
        client: './client.js'
      output:
        path: path.resolve(__dirname,'dist')
        filename: '[name].bundle.js'
        chunkFilename: '[name].bundle.js'
        library: 'Application'
        libraryTarget: 'umd'
      module:
        rules: [
          {
            test: /src\/.*\.coffee\.md$/
            use: [
              {loader: 'surplus-loader'}
              {loader: 'coffee-loader', options: literate:true}
            ]
          }
        ]
      optimization:
        minimizer: [
          new TerserPlugin
            parallel: true
            extractComments: 'all'
        ]
      plugins: [

        new webpack.DefinePlugin 'process.env.NODE_ENV': JSON.stringify 'production'

      ]
      devServer:
        contentBase: './dist'
